import java.util.*;
import java.lang.Math;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
   }

   private long a;
   private long b;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if(b <= 0) throw new RuntimeException("Denominator can't be smaller or equals to 0!");
      this.a = a;
      this.b = b;
      long gcd =findGCD(a,b);
      if(gcd >0)
      {  this.a = a / gcd;
         this.b = b/ gcd;
      }
      int neg = 0;
      if (this.a < 0){
         neg ++;
         this.a = this.a * -1;
      } else if (this.b <= 0){
         throw new RuntimeException("Denominator can not be negative or zero: " + this.b);
      }
      if(neg > 0){
         this.a = this.a * -1;
      }
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return a;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return b;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
       if(b <= 0) throw new RuntimeException("Denominator can not be negative or zero: " + b);
      return  a + "/" + b;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction obj = (Lfraction) m;
      return this.compareTo(obj) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.a, this.b);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long num = this.a * findLCM(this.b, m.b) / this.b + m.a * findLCM(this.b, m.b) / m.b;
      return new Lfraction(num, findLCM(this.b, m.b));
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
       return new Lfraction(m.a * this.a, m.b * this.b);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      long num = this.b;
      long den = this.a;
      if(num < 0 && den < 0 || num > 0 && den < 0){
          num = num * -1;
          den = den * -1;
      }else if (den <= 0){
          throw new RuntimeException("Denominator can not be negative or zero: " + den);
      }
      return new Lfraction(num, den);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(this.a * -1, this.b);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus(Lfraction m) {
      if(m.b == 0 || m.a == 0) throw new RuntimeException("Not possible to divide by 0");
      return this.plus(m.opposite());
   }


   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
       if(m.b == 0 || m.a == 0) throw new RuntimeException("Not possible to divide by 0");
       Lfraction obj = m.inverse();
       return this.times(obj);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      double d = this.a * m.b - this.b * m.a;
      if (d < 0){
         return -1;
      }else if (d > 0){
         return 1;
      }
      return 0;

   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.a, this.b);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long) this.toDouble();

   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(this.integerPart() != 0 ?
              this.a - this.b * this.integerPart():this.a, this.b);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.a / (double)this.b;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
       if(d > 0){
           double num = Math.round((double) d * f);
           return new Lfraction((long) num, d);
       }
       throw new RuntimeException("Denominator can not be negative or zero: " + d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] parts = s.split("/");
      if(parts.length != 2){
          throw new RuntimeException(s + " is illegal string to create a fraction");
      }
      long num = 0;
      long den = 0;
      try{
         num = Long.parseLong(parts[0]);
         den = Long.parseLong(parts[1]);
      }catch(IllegalArgumentException e){
         System.out.println(s + " is illegal string to create a fraction");
      }

      if (den == 0){
          throw new RuntimeException("Denominator can not be zero: " + s);
      }else if(num < 0 && den < 0 || num > 0 && den < 0){
          num = num * -1;
          den = den * -1;
      }else if(den < 0){
          throw new RuntimeException("Denominator can not be negative: " + s);
      }

      return new Lfraction(num, den);
   }

   /** Method to find greatest common divider.
    * @param num numerator
    * @param den denominator
    * @return greatest common divider
    * Used source: https://www.javatpoint.com/java-program-to-find-gcd-of-two-numbers
    */
   private static long findGCD(long num, long den) {
      if (den == 0)
         return Math.abs((int)num);
      return findGCD(den, (long)num % den);
   }

   /** Method to find largest common multiple.
    * @param a denominator
    * @param b denominator
    * @return largest common multiple
    * Used source: https://www.baeldung.com/java-least-common-multiple
    */
   private static long findLCM(long a, long b) {
      if (a == 0 || b == 0) {
          throw new RuntimeException("Denominator can not be negative or zero");
      }
      long higherNumber = Math.max(Math.abs(a), Math.abs(b));
      long lowerNumber = Math.min(Math.abs(a), Math.abs(b));
      long lcm = higherNumber;
      while (lcm % lowerNumber != 0) {
         lcm += higherNumber;
      }
      return lcm;
   }
   /** Method to find largest common multiple.
    * @param grade - power to raise a number.
    * @return raised fraction.
    */
   public Lfraction pow(int grade){
      if (grade == 0){
         return  new Lfraction(1, 1);
      }
      if (grade ==1){
         return this;
      }
      if (grade ==-1){
         return this.inverse();
      }
      if (grade > 0){
         return this.times(this.pow(grade-1));
      }else {
        return this.pow(grade * -1).inverse();
      }
   }
}

